﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using search_app.Properties;

namespace search_app
{
    public partial class Form1 : Form
    {
        bool isNext = false;
        public Form1()
        {
           
            InitializeComponent();
            button2.Enabled = false;
            //Загружаем значения текстовых полей из прошлого запуска
            textBox1.Text = Settings.Default["direct_box"].ToString();
            textBox2.Text = Settings.Default["format_box"].ToString();
            textBox3.Text = Settings.Default["text_box"].ToString();
            
        }

        
        
        private void button1_Click_1(object sender, EventArgs e)
        {
            int count = 0;
            treeView1.Nodes.Clear();
            string path = textBox1.Text;
            string file_type = textBox2.Text;
            string text_in_file = textBox3.Text;
            int click_count = 0;
            int search_count=0;
            lbl_file.Text = "";
            lbl_count_srch.Text = "0";
            button2.Enabled = true;
            try
            {

                TreeNode rootNode = new TreeNode(path);
                isNext = true;
                if (!String.IsNullOrEmpty(path) && !String.IsNullOrEmpty(file_type) && !String.IsNullOrEmpty(text_in_file))
                {
                    //получаем файлы
                    timer1.Start();
                    string[] files = Directory.GetFiles(path);
                    for (int i = 0; i < files.Length; i++)
                    {
                        if (isNext)
                        {
                            if (files[i].EndsWith(file_type))
                            {

                                using (FileStream fstream = File.OpenRead(files[i]))
                                {
                                    // преобразуем строку в байты
                                    byte[] array = new byte[fstream.Length];
                                    // считываем данные
                                    fstream.Read(array, 0, array.Length);
                                    // string[] textFromFile = (System.Text.Encoding.Default.GetString(array)).Split(new char[] { ' ' }); 
                                    string textFromFile = System.Text.Encoding.Default.GetString(array);
                                    //ecли подстрока есть в каком то файле то добавим сей файл в список
                                    if ((textFromFile.IndexOf(text_in_file) != -1))
                                    {
                                        search_count++;
                                        rootNode.Nodes.Add(new TreeNode(files[i]));
                                    }
                                }
                                lbl_count_srch.Text = search_count.ToString();

                            }
                            lbl_file.Text = files[i];
                            count++;
                            label5.Text = count.ToString();
                        }
                        else
                        {
                            break;
                        }
                    }
                    treeView1.Nodes.Add(rootNode);
                    rootNode.Expand();
                    //Без строчки ниже время будет идти, пока не будет нажата кнопка СТОП
                    //timer1.Stop();//Останавливаем таймер, получится время потраченное на процеесс....
                    
                    click_count++;
                    if (click_count > 0)
                    {
                        button1.Text = "Новый поиск";//меняем название кнопки
                        button2.Text = "Стоп";
                        isNext = false;
                        date_now = new DateTime(0, 0);//зануляем таймер
                        //Изменяем значения текстовых полей и сохраняем
                        Settings.Default["direct_box"] = textBox1.Text;
                        Settings.Default["format_box"] = textBox2.Text;
                        Settings.Default["text_box"] = textBox3.Text;
                        Settings.Default.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                //вывод сообщения при ошибке
                MessageBox.Show(ex.ToString(), "Ошибка:", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
        }

       
        private void textBox1_Validating_1(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text))
            {
                errorProvider1.SetError(textBox1, "Корневая папка не указана");
            }
            else
            {
                errorProvider1.Clear();
            }
        }

        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            
            if (String.IsNullOrEmpty(textBox2.Text))
            {
                errorProvider2.SetError(textBox2, "укажите формат");
            }
            
            else
            {
                errorProvider2.Clear();
            }
        }

        private void textBox3_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrEmpty(textBox3.Text))
            {
                errorProvider3.SetError(textBox3, "введите текст для поиска");
            }

            else
            {
                errorProvider3.Clear();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Text = "Продолжить";
            if (timer1.Enabled == true)
            {
                button2.Text = "Продолжить";
                timer1.Stop();
            }
            else
            {
                button2.Text = "Стоп";
                isNext = true;
                timer1.Start();
            }
            
        }
        DateTime date_now = new DateTime(0,0);
        private void timer1_Tick(object sender, EventArgs e)
        {
            date_now = date_now.AddSeconds(1);
            label7.Text = date_now.ToString("mm:ss");
        }
    }
}
